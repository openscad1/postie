echo make openscad.tidy
ls postie-*.scad \
  | sed 's/scad$/stl/' \
  | sed 's,^,make openscad.artifacts/,'
