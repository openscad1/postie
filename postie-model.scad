include <postie.scad>;


// stacked
translate([ 0, 0,  0])  innie();
translate([ 0, 0, 15])  outie();
translate([ 0, 0, 10]) middle();



// in a row
translate([  0, 50,  0])  innie();
translate([ 50, 50, 15])  outie();
translate([100, 50, 10]) middle();

