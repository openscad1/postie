// NOSTL

include <libopenscad/mcube.scad>;
include <libopenscad/mhole.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/mtube.scad>;
include <libopenscad/mhole.scad>;

include <libthread/threadlib/threadlib.scad>;


default_base_size = 30;
default_base_height_ratio = 1/3;
default_base_chamfer_ratio = 1/8;

default_clamp_plate_margin = 1;
default_clamp_height_ratio = 1/6;

default_bolt_scale = [0.975, 0.975, 1.25];
default_bolt_turns = 6;

default_nut_scale = [1, 1, 1.25];
default_nut_turns = 2;

default_peg_diameter = 7;
default_peg_offset = 8;
default_peg_margin = 1;

default_thread_identifier = "M24";

default_fn = 360;

module base(
        base_size = default_base_size, 
        base_height_ratio = default_base_height_ratio, 
        base_chamfer_ratio = default_base_chamfer_ratio,
        bolt_scale = default_bolt_scale,
        bolt_turns = default_bolt_turns,
        peg_diameter = default_peg_diameter,
        peg_offset = default_peg_offset,
        thread_identifier = default_thread_identifier,
        fn = default_fn) {
            
    $fn = fn;
            

    color("lightgreen") {
        hull () { 
            f = 4;
            translate([ base_size/2,  base_size/2, 0]) mcylinder(d = base_size / f, h = base_size * base_height_ratio, align = [-1, -1, -1],chamfer = base_size * base_height_ratio * base_chamfer_ratio);
            translate([ base_size/2, -base_size/2, 0]) mcylinder(d = base_size / f, h = base_size * base_height_ratio, align = [-1,  1, -1],chamfer = base_size * base_height_ratio * base_chamfer_ratio);
            translate([-base_size/2,  base_size/2, 0]) mcylinder(d = base_size / f, h = base_size * base_height_ratio, align = [ 1, -1, -1],chamfer = base_size * base_height_ratio * base_chamfer_ratio);
            translate([-base_size/2, -base_size/2, 0]) mcylinder(d = base_size / f, h = base_size * base_height_ratio, align = [ 1,  1, -1],chamfer = base_size * base_height_ratio * base_chamfer_ratio);
        }
    }        
}




module pegs(
        base_size = default_base_size, 
        base_height_ratio = default_base_height_ratio, 
        base_chamfer_ratio = default_base_chamfer_ratio,
        bolt_scale = default_bolt_scale,
        bolt_turns = default_bolt_turns,
        peg_diameter = default_peg_diameter,
        peg_offset = default_peg_offset,
        thread_identifier = default_thread_identifier,
        fn = default_fn) {
            
    $fn = fn;
            
    union() {
        intersection() {
            
            scale(bolt_scale) {
                bolt(thread_identifier, turns = bolt_turns);
            }

            color("salmon") {
                union() {
                    for (a = [45 : 90 : 360]) {
                        rotate([0, 0, a]) {
                            translate([peg_offset, 0, 0]) {
                                cylinder(d=peg_diameter, h=100, center = true);
                            }
                        }
                    }
                }
            }

        }

    }
}





module innie(
        base_size = default_base_size, 
        base_height_ratio = default_base_height_ratio, 
        base_chamfer_ratio = default_base_chamfer_ratio,
        bolt_scale = default_bolt_scale,
        bolt_turns = default_bolt_turns,
        peg_diameter = default_peg_diameter,
        peg_offset = default_peg_offset,
        thread_identifier = default_thread_identifier,
        fn = default_fn, 
        copies = 1, rows = 1) {
            
    color("lightgreen") {
        hull() {
            for(c = [1,rows]) {
                translate([0, base_size * c, 0]) {
                    for(c = [1,copies]) {
                        translate([base_size * c, 0, 0]) {
                            base(
                                base_size          = base_size, 
                                base_height_ratio  = base_height_ratio, 
                                base_chamfer_ratio = base_chamfer_ratio,
                                bolt_scale         = bolt_scale,
                                bolt_turns         = bolt_turns,
                                peg_diameter       = peg_diameter,
                                peg_offset         = peg_offset,
                                thread_identifier  = thread_identifier,
                                fn                 = fn
                            );
                        }
                    }
                }
            }
        }
    }
    
    
    for(c = [1:rows]) {
        translate([0, base_size * c, 0]) {
            for(c = [1:copies]) {
                translate([base_size * c, 0, 0]) {
                    pegs(
                        base_size          = base_size, 
                        base_height_ratio  = base_height_ratio, 
                        base_chamfer_ratio = base_chamfer_ratio,
                        bolt_scale         = bolt_scale,
                        bolt_turns         = bolt_turns,
                        peg_diameter       = peg_diameter,
                        peg_offset         = peg_offset,
                        thread_identifier  = thread_identifier,
                        fn                 = fn
                    );
                }
            }       
        }
    }
            
}





module middle(
        base_size = default_base_size, 
        base_height_ratio = default_base_height_ratio, 
        base_chamfer_ratio = default_base_chamfer_ratio,
        clamp_plate_margin = default_clamp_plate_margin,
        clamp_height_ratio = default_clamp_height_ratio,
        peg_diameter = default_peg_diameter,
        peg_offset = default_peg_offset,
        peg_margin = default_peg_margin,
        fn = default_fn) {
            
    $fn = fn;
            
color("pink")    
    difference() {
        color("lightgreen") {
            mcylinder(d    = base_size - clamp_plate_margin, h = (base_size - clamp_plate_margin) * clamp_height_ratio, align = [0, 0, 0], chamfer = base_size * clamp_height_ratio * base_chamfer_ratio);

        }

        color("salmon") {
            union() {
                for (a = [45 : 90 : 360]) {
                    rotate([0, 0, a]) {
                            translate([peg_offset, 0, 0]) {
//                            cylinder(d=peg_diameter + peg_margin, h=100, center = true);
                                
                            mhole(diameter = peg_diameter + peg_margin, height = (base_size - clamp_plate_margin) * clamp_height_ratio,                    chamfer = base_size * clamp_height_ratio * base_chamfer_ratio);




                        }
                    }
                }
            }
        }

    }
    
    /*

        color("red") {
            union() {
                for (a = [45 : 90 : 360]) {
                    rotate([0, 0, a]) {
                            translate([peg_offset, 0, 0]) {
                                mhole(height = (base_size - clamp_plate_margin) * clamp_height_ratio, diameter = peg_diameter + peg_margin, chamfer = (peg_diameter + peg_margin) / 8);
                        }
                    }
                }
            }
        }
*/

}

module outie(
        base_size = default_base_size, 
        base_height_ratio = default_base_height_ratio, 
        base_chamfer_ratio = default_base_chamfer_ratio,
        nut_scale = default_nut_scale,
        thread_identifier = default_thread_identifier,
        nut_turns = default_nut_turns,
        clamp_plate_margin = default_clamp_plate_margin,
        fn = default_fn) {
            
    $fn = fn;
            
    specs = thread_specs(str(default_thread_identifier, "-int"), table=THREAD_TABLE);
    P = specs[0];
    H = (nut_turns + 1) * P * nut_scale.z;
    Dsup = specs[2];

    echo("P", P);
    echo("H", H);
    echo("Dsup", Dsup);
            
    Douter = base_size - clamp_plate_margin;
    chamfer = base_size * default_base_height_ratio * base_chamfer_ratio;

    difference() {

        intersection() {    
            difference() {
                color("cyan") {
                    translate([0, 0, -H/2]) {
                        scale(nut_scale) {
                            translate([0, 0, P/2]) {
                                nut(thread_identifier, turns = nut_turns, Douter = Douter, fn = fn, higbee_arc=90);
                            }
                        }
                    }
                }
                       
                color("red") {
                    for(z = [0 : 30 : 360]) {
                        rotate([0, 0, z]) {
                            translate([Douter/2 + chamfer * 2/3, 0, 0]) {
                                mcylinder(d = chamfer * 2, h = H + 1, align = [0, 0, 0]);
                            }
                        }
                    }
                }
            }
            mcylinder(h = H, d = Douter, chamfer = chamfer / 2, align = [0, 0, 0]);
        }
        {
                specs = thread_specs(str(default_thread_identifier, "-ext"), table=THREAD_TABLE);
            P = specs[0];
            H = (nut_turns + 1) * P * nut_scale.z;
            Dsup = specs[2];

            echo("P", P);
            echo("H", H);
            echo("Dsup", Dsup);

            mhole(height = H, diameter = Dsup, chamfer = chamfer * 2);
        }

    }

}




module ring(
        base_size = default_base_size, 
        base_height_ratio = default_base_height_ratio, 
        base_chamfer_ratio = default_base_chamfer_ratio,
        fn = default_fn) {
            
    $fn = fn;
            
    mtube(od = base_size, id = base_size * .8, h = 20, chamfer = 1);
            
}

//ring();

