# libthread

## home, sweet home

    git@gitlab.com:openscad1/libthread.git


## adopt me

    git subtree add  --message 'mtools git subtree add locator - libthread\nrepo=git@gitlab.com:openscad1/libthread.git\nprefix=libthread\nbranch=main' --prefix libthread  git@gitlab.com:openscad1/libthread.git main --squash


## inherit updates from the mother ship

    git subtree pull --prefix libthread git@gitlab.com:openscad1/libthread.git main --squash


## push our updates to the mother ship

    git subtree push --prefix libthread git@gitlab.com:openscad1/libthread.git main --squash

